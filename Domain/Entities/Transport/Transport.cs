﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Domain.Entities
{
    ///
    ///Показывает транспорт используемый заказчиком.
    ///Из ТТН и доверенного
    ///

    [Table("bTrans")]
    public class Transport
    {        
        [Key]
        [DisplayName("Гос. №")]
        public string Gn { get; set; }
        [DisplayName("Модель")]
        public string TecModel { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? CustID { get; set; }
        public DateTime Dat { get; set; }
    }
}
