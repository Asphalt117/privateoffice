﻿using Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using Domain.ModelView;
namespace Domain.Repository
{
    public class CustRepository
    {
        private readonly AbzContext db = new AbzContext();

        public IEnumerable<Cust> GetINN(string inn)
        {
            return db.Custs.Where(d => d.Inn == inn);
        }

        public Cust GetInn(string inn)
        {
            return db.Custs.FirstOrDefault(d => d.Inn == inn);
        }
        public Cust Get1S(string Cod)
        {
            return db.Custs.FirstOrDefault(d => d.Cod1s == Cod);
        }
        public Cust Get(int id)
        {
            return db.Custs.FirstOrDefault(c => c.CustId == id);                
        }

        public IEnumerable<OrgView> GetCust(string id)
        {
            var orgView = from u in db.UserInCusts
                          join c in db.Custs
                              on u.CustID equals c.CustId
                          where u.UserId == id                          
                          orderby u.LastDat descending
                          select new OrgView
                          {
                              txt = c.FullName,
                              ID = c.CustId
                          };

            return orgView;
        }

    }
}
