﻿using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.ModelView;
using System.Web.Mvc;
using System;
using System.Data.Entity;
using Domain.Engine;
using System.Threading.Tasks;
using System.Data;

namespace Domain.Repository
{
    public class TransportRepository
    {
        private readonly AbzContext db = new AbzContext();
        private int custID;

        public TransportRepository(int cust)
        {
            custID = cust;
        }

        public async Task<List<Transport>> GetList()
        {
            return await db.Transports.Where(t => t.CustID == custID).OrderByDescending(t=>t.Dat).ToListAsync();
        }
        public async Task<Transport> Get(string gn)
        {
            return await db.Transports.Where(t => t.CustID == custID).FirstOrDefaultAsync(o => o.Gn == gn);
        }

        public async Task<int> Save(Transport trans)
        {
            //Сохранение в доверенном, в деталях
            TrustTec tec = new TrustTec();
            tec.CustId = custID;
            tec.DatCreate = DateTime.Now;
            db.TrustTecs.Add(tec);
            await db.SaveChangesAsync();

            TrustTecDet tecDet = new TrustTecDet();
            tecDet.Gn = trans.Gn;
            tecDet.TrustTecId = tec.TrustTecId;
            tecDet.TecModel = trans.TecModel;
            
            db.TrustTecDets.Add(tecDet);
            return await db.SaveChangesAsync();
        }
    }
}