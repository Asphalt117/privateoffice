﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using System.Threading.Tasks;


namespace Domain.Repository
{
    public class TtnRepository
    {
        private AbzContext db = new AbzContext();
        public List<Ttn> GetTtn(int custId, DateTime begDt, DateTime endDt)
        {
            return db.Ttns.Where(w => w.CustID == custId && w.Dat >= begDt
                                           && w.Dat < endDt)
                    .OrderBy(b => b.Num).ToList();
        }

        public IQueryable<Ttn> GetSkipTake(int skip, int take, int custId,DateTime begDt,DateTime endDt)
        {
            return db.Ttns.Where(w => w.CustID == custId && w.Dat >= begDt
                                           && w.Dat < endDt)
                    .OrderBy(b => b.Num)
                    .Skip(skip)
                    .Take(take);
        }

        public int GetCount(int custId, DateTime begDt, DateTime endDt)
        {
            return db.Ttns.Count(t => t.CustID == custId
                                           && t.Dat >= begDt
                                           && t.Dat < endDt);

        }

    }
}
