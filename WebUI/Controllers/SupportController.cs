﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using System.Data.Entity;

namespace WebUI.Controllers
{
    public class SupportController : BaseController
    {
        public ActionResult Index()
        {
            Menu.ChangeSelected(1, 3);
            Session["Menu"] = Menu;
            int cust = Menu.CustId;
            List<Comment> coment = db.Comments.Where(c => c.CustId == cust).ToList();
            return View(coment);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Comment cmt)
        {
            Comment comment = new Comment();
            comment.Note = cmt.Note;
            comment.CustId = Menu.CustId;
            db.Comments.Add(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
            //return View("CommentOK");
        }
        public ActionResult Answer(int id)
        {
            Comment comment = db.Comments.Find(id);
            return View(comment);
        }
        [HttpPost]
        public ActionResult Answer(Comment cmt)
        {
            Comment comment = db.Comments.Find(cmt.CommentId);
            comment.Response = cmt.Response;
            db.Entry(comment).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}