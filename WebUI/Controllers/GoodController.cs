﻿using Domain.Entities;
using Domain.Repository;
using System.Linq;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class GoodController : Controller
    {
        private GoodRepository repo = new GoodRepository();
        private static int pagesize = 20;          // Items count on one page
        private static int vsblpagescount = 10;    // Visible page numbers in page navigator central list 
        private AbzContext db = new AbzContext();
        
        public ActionResult Index(string act,string cont,int categid = 2, int pageNum = 0 )
        {
            ViewBag.MenuItem = "good";
            ViewBag.MenuId = categid;
            ViewBag.ProductDetail = false;
            GoodList goodlist = new GoodList();
            var countitems = repo.GetCountItems(categid);
            goodlist.CategId = categid;
            goodlist.CategName = db.Categs.Find(categid).txt;
            goodlist.PageInfo = new PageInfo { pageNum = pageNum, itemsCount = countitems, pageSize = pagesize, vsblPagesCount = vsblpagescount };
            goodlist.Products = repo.GetSkipTake(pageNum * pagesize, pagesize, categid).ToList();
            if (act != null)
            {
                goodlist.Action = act;
                goodlist.Controller = cont;
                Session["gl"] = goodlist;
            }
            else
            {
                GoodList gl = (GoodList)Session["gl"];
                goodlist.Action = gl.Action;
                goodlist.Controller = gl.Controller;
            }
            return View(goodlist);
        }
        public ActionResult CategoryMenu(string menuName, int? menuId)
        {
            var assortmentMenu = new AssortmentMenu();
            assortmentMenu.Categs = db.Categs;
            assortmentMenu.MenuName = menuName;
            assortmentMenu.MenuId = menuId;
            return PartialView(assortmentMenu);
        }
    }
}