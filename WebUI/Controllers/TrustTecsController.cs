﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using Domain.Repository;
using WebUI.Models;
using Domain.ModelView;

namespace WebUI.Controllers
{
    public class TrustTecsController : BaseController
    {
        private TrustTecsRepository repo;
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            Menu = (MyMenu)Session["Menu"];
            //Cust = db.Custs.FirstOrDefault(c => c.CustId == Menu.CustId);
            repo = new TrustTecsRepository(Cust.CustId);
        }
        public async Task<ActionResult> Index()
        {
            Menu.ChangeSelected(2, 4);
            Session["Menu"] = Menu;            
            return View(await repo.Get());
        }
        public ActionResult CreateTransport()
        {
            TrustTec tec = new TrustTec();
            tec.CustId = Menu.CustId;
            List<TrustTecDet> det = new List<TrustTecDet>();

            if (Session["TrustTec"] != null)
                tec = (TrustTec)Session["TrustTec"];

            if (Session["TecDet"] != null)
                det = (List<TrustTecDet>)Session["TecDet"];
            else
                Session["TecDet"] = det;

            TrustTecView tecView = new TrustTecView();
            tecView.Tec = tec;
            tecView.Detail = det;
            return View(tecView);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTransport(TrustTec Tec, string tr)
        {
            Session["TrustTec"] = Tec;
            if (tr != null)
                return RedirectToAction("CreateTec");
            if (Session["TecDet"] == null)
                return HttpNotFound();
            TrustTecView tecView = new TrustTecView();
            tecView.Tec = Tec;
            tecView.Detail= (List<TrustTecDet>)Session["TecDet"];
            repo.Save(tecView);

            return RedirectToAction("Index");
        }

        public ActionResult CreateTec()
        {
            return View(new TrustTecDet());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTec(TrustTecDet det)
        {
            if (ModelState.IsValid)
            {
                List<TrustTecDet> tecs;
                if (Session["TecDet"] == null)
                    Session["TecDet"] = new List<TrustTecDet>();

                tecs = (List<TrustTecDet>)Session["TecDet"];
                tecs.Add(det);
                Session["TecDet"] = tecs;
                return RedirectToAction("CreateTransport");
            }
            return HttpNotFound();
        }


        public ActionResult Create()
        {
            TrustTecV tecV;
            if (Session["TrustTecV"] == null)
            {
                tecV = new TrustTecV();
                tecV.Detail = new List<TrustTecDet>();
                Session["TrustTecV"] = tecV;
            }
            else
            {
                tecV = (TrustTecV)Session["TrustTecV"];
            }
            return View(tecV);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TrustTecV tecV,string tr)
        {
            //Не возвращаются детали
            if (ModelState.IsValid && tr == null)
            {      
                TrustTecV tec = (TrustTecV)Session["TrustTecV"];
                tecV.Detail = tec.Detail;
                await repo.Save(tecV);
                return RedirectToAction("Index");
            }
            Session["TrustTecV"] = tecV;
            if (tr != null)
            {
                return RedirectToAction("CreateTec");
            }
            return View(tecV);
        }

        //public ActionResult CreateTec()
        //{
        //    TrustTecDet det;
        //    det = new TrustTecDet();
        //    return View("CreateTec", det);
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult CreateTec(TrustTecDet det)
        //{
        //    TrustTecV tecV = (TrustTecV)Session["TrustTecV"];
        //    if (tecV.Detail == null)
        //        tecV.Detail = new List<TrustTecDet>();
        //    tecV.Detail.Add(det);
        //    Session["TrustTecV"] = tecV;
        //    return RedirectToAction("Create");            
        //}

         public async Task<ActionResult> Delete(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrustTecDet tecDet= await db.TrustTecDets.FindAsync(id);
                
            if (tecDet == null)
            {
                return HttpNotFound();
            }
            return View(tecDet);
        }

        // POST: OrgTrustTecs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            repo.delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
