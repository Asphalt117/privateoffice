﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Domain.Entities;
using Domain.ModelView;
using WebUI.Models;
using Domain.Repository;
using Calabonga.Xml.Exports;
using WebUI.Infrastructure;
using System.Threading.Tasks;
using System.IO;

namespace WebUI.Controllers
{
    public class BalancesController : BaseController
    {
        public int year = DateTime.Today.Year;
        public int dm = DateTime.Today.Month;
        private static int pagesize = 20;          // Items count on one page
        private static int vsblpagescount = 10;    // Visible page numbers in page navigator central list 


        public void SetDat(int? selectedId, int? Id)
        {
            if (Id != null)
                year = (int)Id;
            else
            {
                if (Session["Year"] != null)
                    year = (int)Session["Year"];
                else
                    year = DateTime.Today.Year;
            }
            if (selectedId != null)
                dm = (int)selectedId;
            else
            {
                if (Session["Month"] != null)
                    dm = (int)Session["Month"];
                else
                    dm = DateTime.Today.Month;
            }
            Session["Month"] = dm;
            Session["Year"] = year;
        }
        [Authorize]
        public ActionResult Index(int? selectedId, int? Id, int pageNum = 1)
        {
            Menu.ChangeSelected(3, 2);
            Session["Menu"] = Menu;
            SetDat(selectedId, Id);
            DateOf datof = new DateOf(dm, year);
            ViewData["Month"] = new SelectList(datof.getMonth, "ID", "Mnth", dm);
            ViewData["Year"] = new SelectList(datof.getYear, "ID", "Mnth", year);
            return View();
        }
        public ActionResult SelectTtn(int? selectedId, int? Id, int pageNum = 0)
        {
            TtnRepository repo = new TtnRepository();
            SetDat(selectedId, Id);
            //Установили 1е число
            DateTime begDt = new DateTime(year, dm, 01, 0, 0, 0);
            //Установили 1е число следующего месяца
            DateTime endDt = begDt.AddMonths(1);
            TtnList ttn = new TtnList();
            int count = repo.GetCount(Menu.CustId, begDt, endDt);
            ttn.PageInfo = new PageInfo { pageNum = pageNum, itemsCount = count, pageSize = pagesize, vsblPagesCount = vsblpagescount };
            ttn.Ttns = repo.GetSkipTake((pageNum)*pagesize, pagesize, Menu.CustId, begDt, endDt).ToList();
            return PartialView(ttn);
        }

        //public async Task<ExcelResult> Export(DateTime dateStart, DateTime dateEnd)
        public ExcelResult Export()
        {
            const string fileName = "Reestr.xls";
            // get data
            int dm = (int)Session["Month"];

            int year = (int)Session["Year"];

            //Установили 1е число
            DateTime begDt = new DateTime(year, dm, 01, 0, 0, 0);
            //Установили 1е число следующего месяца
            DateTime endDt = begDt.AddMonths(1);
            TtnRepository repo = new TtnRepository();
            List<Ttn> ttn = repo.GetTtn(Cust.CustId, begDt, endDt);

            var items = repo.GetTtn(Cust.CustId, begDt, endDt);
            var title = $"Реестр отрузки Абз-4 с {begDt.ToShortDateString()} по {endDt.ToShortDateString()}";
            var html = ExportBuilder.Build(items, title);

            // prepare virtual file
            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(html);
                writer.Flush();
                stream.Position = 0;

                // Create memory file to send by email
                //using (var fileImage = new MemoryFile(stream, "application/vnd.ms-excel", fileName))
                //{
                //    //Sending email
                //    _emailService.SendMail(emailFrom, emailTo, title, title, new List<HttpPostedFileBase> { fileImage });
                //}
            }
            return new ExcelResult(fileName, html);
        }

        [Authorize]
        public ActionResult Balance()
        {
            BalanceRepository repo = new BalanceRepository();            
            Menu.ChangeSelected(3, 1);
            Session["Menu"] = Menu;
            return View(repo.GetListBalance(Menu.CustId));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
