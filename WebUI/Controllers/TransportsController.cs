﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using WebUI.Models;
using Domain.Repository;

namespace WebUI.Controllers
{
    public class TransportsController : BaseController
    {
        private TransportRepository repo;
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            Menu = (MyMenu)Session["Menu"];
            Cust = db.Custs.FirstOrDefault(c => c.CustId == Menu.CustId);
            repo = new TransportRepository(Cust.CustId);
        }
            public async Task<ActionResult> Index(string act, string cont)
        {
            TransportList trans = new TransportList();
            trans.Trans = await repo.GetList();                
            if (act != null)
            {
                trans.Action = act;
                trans.Controller = cont;
            }
            else
            {
                Menu.ChangeSelected(2, 7);
                trans.Action = "Index";
                trans.Controller = "Transports";
            }
            Session["trans"] = trans;
            return View(trans);
        }

        public ActionResult Create()
        {
            return View();
        }

        // POST: Transports/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Gn,TecModel")] Transport transport)
        {
            if (ModelState.IsValid)
            {
                TransportList trans = (TransportList)Session["trans"];
                int tr= await repo.Save(transport);   
                return RedirectToAction(trans.Action,trans.Controller);
            }

            return View(transport);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
