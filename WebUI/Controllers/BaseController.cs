﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class BaseController : Controller
    {
        public MyMenu Menu;
        public Cust Cust;
        public AbzContext db = new AbzContext();
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            Menu = (MyMenu)Session["Menu"];
            Cust= db.Custs.FirstOrDefault(c => c.CustId == Menu.CustId);
        }
    }
}