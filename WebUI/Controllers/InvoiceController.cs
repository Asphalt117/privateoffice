﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using Domain.ModelView;
using Domain.Repository;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class InvoiceController : BaseController
    {
        private OrderRepository repo = new OrderRepository();
        private OrderView order;
        private OrderSes orderSes = new OrderSes();
        public async Task<ActionResult> Index()
        {
            Menu.ChangeSelected(4, 2);
            Session["Menu"] = Menu;
            Session["ChDetail"] = null;
            Session["OrdSes"] = null;
            Session["Detail"] = null;
            List<OrderV> orders = await repo.GetOrder(Menu.CustId,1);
            return View(orders);
        }
        
        public async Task<ActionResult> CreateFrom(int id)
        {
            order = await repo.GetCopy(id,1);
            order.Invoice = 1;
            orderSes.id = order.OrderId;
            orderSes.act = "Новый счет";
            Session["OrdSes"] = orderSes;
            return RedirectToAction("Booking");
        }
        public async Task<ActionResult> Create()
        {
            order = await repo.GetCreate(Menu.CustId,1);
            orderSes.id = order.OrderId;
            orderSes.act = "Новый счет";
            Session["OrdSes"] = orderSes;
            return RedirectToAction("Booking");
        }
        public ActionResult Edit(int id)
        {
            orderSes.id = id;
            orderSes.act = "Редактировать счет";
            Session["OrdSes"] = orderSes;

            return RedirectToAction("Booking");
        }
        public async Task<ActionResult> Copy(int id)
        {
            order = await repo.GetCopy(id);
            orderSes.id = order.OrderId;
            orderSes.act = "Копировать счет";
            Session["OrdSes"] = orderSes;
            return RedirectToAction("Booking");
        }
        public async Task<ActionResult> Delete(int id)
        {
            order = await repo.GetChange(id);
            orderSes.id = id;
            orderSes.act = "Удаление счета";
            Session["OrdSes"] = orderSes;

            ViewBag.Order = "Удаление счета";
            return View(order);
        }

        [HttpPost]
        public async Task<ActionResult> Delete()
        {
            orderSes = (OrderSes)Session["OrdSes"];
            await repo.Delete(orderSes.id);
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Booking()
        {
            orderSes = (OrderSes)Session["OrdSes"];
            order = await repo.GetChange(orderSes.id);
            Session["Detail"] = order.Products;
            ViewBag.Order = orderSes.act;
            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Booking(OrderView ord, string Gd, string Adr, string Prs)
        {
            List<OrderProductView> detail = (List<OrderProductView>)Session["Detail"];
            ord.Invoice = 1;
            if (ModelState.IsValid && Gd == null && Adr == null && Prs == null && ord.StatusId == 0)
            {
                ord.StatusId = 1;
            }
            await repo.Save(ord, detail);
            if (Gd != null)
            {
                return RedirectToAction("Index", "Good", new { act = "Detail", cont = "Order" });
            }
            if (Adr != null)
                return RedirectToAction("NewAdres");
            if (Prs != null)
                return RedirectToAction("NewContact");

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> DelDetail(int id)
        {
            Session["ChDetail"] = id;
            OrderProductView detail = await db.OrderProductViews.FindAsync(id);
            return View(detail);
        }
        [HttpPost]
        public async Task<ActionResult> DelDetail()
        {
            int id = (int)Session["ChDetail"];
            OrderProduct prod = await db.OrderProducts.FindAsync(id);
            db.OrderProducts.Remove(prod);
            await db.SaveChangesAsync();
            Session["ChDetail"] = null;
            return RedirectToAction("Index");
        }
        public ActionResult ChDetail(int id, string act)
        {
            Session["ChDetail"] = id;
            return RedirectToAction("Index", "Good", new { act = "Detail", cont = "Order" });
        }
        public async Task<ActionResult> Detail(int id)
        {
            Good good = await db.Goods.FindAsync(id);
            OrderProductView svd = new OrderProductView();
            svd.GoodId = good.GoodID;
            svd.Good = good.txt;
            svd.Unit = good.Unit;
            svd.OrderId = id;
            if (Session["ChDetail"] != null)
            {
                int DetId = (int)Session["ChDetail"];
                Session["ChDetail"] = null;
                svd.OrderProductId= DetId;                
            }
            return View(svd);
        }

        [HttpPost]
        public async Task<ActionResult> Detail(OrderProductView svd)
        {
            orderSes = (OrderSes)Session["OrdSes"];
            if (Session["Detail"] != null)
            {
                await repo.SaveDetail(orderSes.id, svd);
            }
            return RedirectToAction("Booking", "Order", new { id = orderSes.id });
        }

        public ActionResult NewContact()
        {
            Person person = new Person();
            person.CustId = Menu.CustId;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> NewContact(Person person)
        {
            try
            {
                person.CustId = Menu.CustId;
                db.Persons.Add(person);
                await db.SaveChangesAsync();

                orderSes = (OrderSes)Session["OrdSes"];
                order = await repo.GetChange(orderSes.id);
                order.PersonId = person.PersonId;
                await repo.Save(order);
                return RedirectToAction("Booking");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult NewAdres()
        {
            Adres adres = new Adres();
            return View(adres);
        }
        [HttpPost]
        public async Task<ActionResult> NewAdres(Adres adres)
        {
            adres.CustId = Menu.CustId;
            db.Adreses.Add(adres);
            await db.SaveChangesAsync();
            orderSes = (OrderSes)Session["OrdSes"];
            order = await repo.GetChange(orderSes.id);
            order.AdresId = adres.AdresID;
            await repo.Save(order);

            return RedirectToAction("Booking");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
    
