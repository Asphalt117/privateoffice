﻿using System.Web.Mvc;
using Domain.Entities;
using Domain.Repository;
using System.Collections.Generic;
using Domain.ModelView;
using System.Linq;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class TrustTecController : BaseController        
    {
        //Сразу показывать список выбранной техники.
        //При создании переходим на спосок использованных машин.
        //Показываем даты и список выбюранных, с возможностью добавить
        private TrustTecRepository repo;

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            Menu = (MyMenu)Session["Menu"];
            Cust = db.Custs.FirstOrDefault(c => c.CustId == Menu.CustId);
            repo = new TrustTecRepository(Cust.CustId);
        }
        public ActionResult Index()
        {
            Menu.ChangeSelected(2, 4);
            Session["Menu"] = Menu;

            //Показываю действующие доверенности
            return View(repo.GetTrustActing());
        }
        public ActionResult CreateTec(TrustTecDet trustTecDet)
        {
            TrustTecDet ttDet = new TrustTecDet();
            if (trustTecDet == null)
                ttDet = new TrustTecDet();
            else
                ttDet = trustTecDet;
            return View(ttDet);
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult CreateTec(TrustTecDet trustTecDet, string qq)
        {
            if (ModelState.IsValid)
            {
                List<TrustTecDet> ttDets = new List<TrustTecDet>();
                //if (Session["TrustTecDet"] == null)
                //{
                //    Session["TrustTecDet"] = ttDets;
                //}

                ttDets = (List<TrustTecDet>)Session["TrustTecDet"];

                ttDets.Add(trustTecDet);
                Session["TrustTecDet"] = ttDets;

                TrustTecView ttView = new TrustTecView();
                ttView.TrustTecDets = ttDets;

                return View("Addtec", ttView);
            }
            return View();
        }
        public ActionResult GNSelect()
        {
            List<TecGn> gn = repo.GetGn().ToList();
            return View(gn);
        }
        public ActionResult AddGN(string id)
        {
            TrustTecDet ttDet = new TrustTecDet();
            ttDet.Gn = id;
            ttDet.TecModel = repo.GetDrivTecs().First(c => c.Gn == id).TecModel;
            return RedirectToAction("CreateTec", "TrustTec", ttDet);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult CreateTec(TrustTecDet trustTecDet,string qq)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        TrustTecDet ttDet = trustTecDet;
        //        db.TrustTecDets.Add(trustTecDet);
        //        db.SaveChanges();

        //        return RedirectToAction("Index");
        //    }
        //    return View();
        //}

        //public ActionResult TecModelSelect()
        //{
        //    return View(repo.GetTecModel(Menu.CustId));
        //}

        //public ActionResult AddTecModel(int id)
        //{
        //    repo = new TrustTecRepository(Cust.OrgCustID);
        //    TrustTecDet ttDet;
        //    TecModel tm = repo.GetTecModel(id);
        //    if (Session["CurTrustTecDet"] != null)
        //    {
        //        ttDet = (TrustTecDet) Session["CurTrustTecDet"];
        //    }
        //    else
        //    {
        //        ttDet=new TrustTecDet();
        //    }
        //    ttDet.TecModel = tm.Name;

        //    Session["CurTrustTecDet"] = ttDet;
        //    return RedirectToAction("CreateTec");
        //}





        //private TrustTecView fillTecView(TrustTecDet ttd)
        //{

        //    TrustTecDet query;    
        //    List<TrustTecDet> ttDets;
        //    if (Session["TrustTecDet"] != null)
        //    {
        //        ttDets = (List<TrustTecDet>)Session["TrustTecDet"];

        //        //Найти наименьший id
        //        query = ttDets.Last();
        //        ttd.TrustTecDetId = query.TrustTecDetId - 1;
        //    }
        //    else
        //    {
        //        ttDets = new List<TrustTecDet>();
        //        ttd.TrustTecDetId = -1;
        //    }

        //    ttDets.Add(ttd);
        //    Session["TrustTecDet"] = ttDets;

        //    TrustTecView ttView = new TrustTecView();
        //    ttView.TrustTecDets = ttDets;
        //    return ttView;
        //}

        //public ActionResult Delete(int id)
        //{
        //    TrustTecView ttView = new TrustTecView();
        //    if (Session["TrustTecDet"] != null)
        //    {
        //        List<TrustTecDet> ttDets = (List<TrustTecDet>)Session["TrustTecDet"];

        //        TrustTecDet ttd = ttDets.Find(n=>n.TrustTecDetId==id);

        //        if (ttd!=null)
        //            ttDets.Remove(ttd);
        //        Session["TrustTecDet"] = ttDets;
        //        ttView.TrustTecDets = ttDets;
        //    }
        //    return View("Addtec", ttView);
        //}

        //public ActionResult AddTec(int id)
        //{
        //    //Добавление транспорта в текущую доверенность
        //    //Перенос из DrivTec в TrustTecDet
        //    TrustTecDet ttd = repo.DrivToTrustTecDets(id);
        //    return View(fillTecView(ttd));
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddTec(TrustTecView tec)
        {
            if (ModelState.IsValid)
            {
                TrustTecView ttView = repo.Add(tec, (List<TrustTecDet>)Session["TrustTecDet"]);
                return View("AddTecSucces", ttView);
            }
            return View();
        }
    }
}