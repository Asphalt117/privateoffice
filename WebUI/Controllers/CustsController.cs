﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using Domain.Repository;
using Domain.ModelView;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class CustsController : Controller
    {
        private AbzContext db = new AbzContext();
        private int CustID;

        [Authorize]
        public async Task<ActionResult> Index(int SelectedId = 0)
        {
            if ((User.IsInRole("Admin")) & (Session["Menu"]!=null))
            {
                MyMenu menu = (MyMenu)Session["Menu"];
                if (SelectedId == 0)
                    CustID = menu.CustId;
                else
                {
                    CustID = SelectedId;
                }
                Cust cust = await db.Custs.FindAsync(CustID);
                menu.ChangeSelected(2, 1);
                menu.CustId = CustID;
                Session["Menu"] = menu;
                CustRepository repo = new CustRepository();
                IEnumerable<OrgView> orgView = repo.GetCust(menu.UserId);

                ViewData["Cust"] = new SelectList(orgView, "ID", "Txt", CustID);
                return View(cust);                
            }
            else
            {
                IList<string> roles = new List<string> { "Роль не определена" };
                ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                                        .GetUserManager<ApplicationUserManager>();
                ApplicationSignInManager SignInManager = HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
                ApplicationUser user = await userManager.FindByEmailAsync(User.Identity.Name);

                if (user != null)
                    roles = await userManager.GetRolesAsync(user.Id);

                if (SelectedId == 0)
                    CustID = user.CustID;
                else
                {
                    CustID = SelectedId;
                    user.CustID = CustID;
                    var result = await userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        SignInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                    }
                }
                Cust cust = await db.Custs.FindAsync(user.CustID);
                MyMenu menu = new MyMenu(roles, cust.SmalName);
                BalanceRepository bl = new BalanceRepository();
                menu.sm = bl.GetBalance(cust.CustId);
                menu.CustId = cust.CustId;
                menu.ChangeSelected(1, 1);
                Session["Menu"] = menu;
                CustRepository repo = new CustRepository();
                IEnumerable<OrgView> orgView = repo.GetCust(user.Id);

                ViewData["Cust"] = new SelectList(orgView, "ID", "Txt", CustID);
                ViewBag.User = user.UserName;
                return View("CustCombo", CustID);
                //return View("CustCombo", cust);
            }
        }

        public ActionResult MyAjax(int SelectedId = 0)
        {
            MyMenu menu = (MyMenu)Session["Menu"];
            if (SelectedId == 0)
                CustID = menu.CustId;
            else
            {
                CustID = SelectedId;
            }
            //Cust cust = await db.Custs.FindAsync(CustID);
            Cust cust = db.Custs.Find(CustID);
            menu.ChangeSelected(1, 1);
            menu.CustId = CustID;
            Session["Menu"] = menu;

            CustRepository repo = new CustRepository();
            IEnumerable<OrgView> orgView = repo.GetCust(menu.UserId);

            ViewData["Cust"] = new SelectList(orgView, "ID", "Txt", CustID);

            return PartialView(cust);

        }
        public ActionResult HeadMenu()
        {
            MyMenu menu = (MyMenu)Session["Menu"];
            if (menu != null)
            {
                ViewBag.cust = menu.Cust;
                ViewBag.sm = menu.sm;
            }

            return PartialView();
        }

        public ActionResult MainMenu(string menuItem)
        {
            MyMenu menu;
            menu = (MyMenu)Session["Menu"];

            if (menu != null)
            {
                ViewBag.cust = menu.Cust;
                ViewBag.sm = menu.sm;
            }
            else
            {
                ViewBag.cust = "";
                menu = new MyMenu(null, null);
                Session["Menu"] = menu;
            }
            IEnumerable<Menu> SubMenu = null;
            foreach (var mn in menu.NavMenu)
            {
                if (mn.Selected)
                {
                    SubMenu = mn.SubMenus;
                }
            }

            menu.SubMenu = SubMenu;

            return PartialView(menu);
        }
          
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
